//
//  ViewController.m
//  proj1-helloWorld
//
//  Created by Vikas on 23/01/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize initialLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clicked:(id)uiLabel{
    [UIView beginAnimations:@"animateText" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:2.0f];
    [self.initialLabel setAlpha:0];
    [initialLabel setText:@"Vikas Koul"];
    [initialLabel setFont:[UIFont fontWithName:@"CourierNewPS-BoldItalicMT" size:24]];
    [initialLabel setTextColor:[UIColor whiteColor]];
    [initialLabel setBackgroundColor:[UIColor blackColor]];
    [self.initialLabel setAlpha:1];
    [UIView commitAnimations];
}

@end
