//
//  AppDelegate.h
//  proj1-helloWorld
//
//  Created by Vikas on 23/01/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

