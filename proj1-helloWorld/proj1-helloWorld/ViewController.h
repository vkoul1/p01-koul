//
//  ViewController.h
//  proj1-helloWorld
//
//  Created by Vikas on 23/01/17.
//  Copyright © 2017 Vikings. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property(nonatomic,strong) IBOutlet UILabel *initialLabel;
-(IBAction)clicked:(id)uiLabel;

@end

